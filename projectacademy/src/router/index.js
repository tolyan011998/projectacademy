import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Supervisor.vue'
import Supervisor from "../views/Supervisor";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Supervisor
  },
  {
    path: '/studentRegistration',
    name: 'studentRegistration',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/StudentRegistration')
  },
  {
    path: '/test',
    name: 'test',
    component: () => import(/* webpackChunkName: "about" */ '../views/Test')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
